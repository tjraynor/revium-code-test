import Vue from 'vue';
import PageArticle from '@/components/Article.vue'

function mountComponentWithProps (Component, propsData) {
  const Constructor = Vue.extend(Component);
  const vm = new Constructor({
    propsData
  }).$mount();

  return vm.$el;
}

describe('PageArticle.vue', () => {
  it('should contain the added title', () => {
    const testTitle = 'New Title';
    const articleData = mountComponentWithProps(PageArticle, { title: testTitle });

    expect(articleData.textContent).toMatch(testTitle);
  });

  it('should contain the added content as a single line', () => {
    const testCopy = 'Some copy';
    const articleData = mountComponentWithProps(PageArticle, { copy: testCopy });

    expect(articleData.textContent).toMatch(testCopy);
  });

  it('should contain the added content as an array', () => {
    const testCopy = ['first line', 'second line'];
    const articleData = mountComponentWithProps(PageArticle, { copy: testCopy });

    expect(articleData.textContent).toMatch(testCopy.join(''));
  });
})
