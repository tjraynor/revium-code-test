import Vue from 'vue';
import Menu from '@/components/Menu.vue'

function mountComponentWithProps (Component, propsData) {
  const Constructor = Vue.extend(Component);
  const vm = new Constructor({
    propsData
  }).$mount();

  return vm.$el;
}

describe('Menu.vue', () => {
  it('should contain the added title', () => {
    const singleItemMenu = [{label: 'Test Label'}];
    const menuData = mountComponentWithProps(Menu, { menuItems: singleItemMenu });

    expect(menuData.querySelector('a').textContent).toMatch('Test Label');
  });

  it('should contain the added url', () => {
    const singleItemMenu = [{url: 'example.com'}];
    const menuData = mountComponentWithProps(Menu, { menuItems: singleItemMenu });

    expect(menuData.querySelector('a').getAttribute('href')).toMatch('example.com');
  });

  it('should contain the both added title and url', () => {
    const singleItemMenu = [{label: 'Test Label', url: 'example.com'}];
    const menuData = mountComponentWithProps(Menu, { menuItems: singleItemMenu });

    expect(menuData.querySelector('a').textContent).toMatch('Test Label');
    expect(menuData.querySelector('a').getAttribute('href')).toMatch('example.com');
  });
})
